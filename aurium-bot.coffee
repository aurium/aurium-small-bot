#!/usr/bin/env coffee

fs = require 'fs'

do (require 'dotenv').config

###
interactionPersonalEmail = (require 'interaction-personal-email')
    groupId: parseInt process.env.EMAIL_GROUP_ID
    user: process.env.EMAIL_USER
    passwd: process.env.EMAIL_PASSWD
    host: process.env.EMAIL_HOST
    dataDir: __dirname + '/data'
###

SmallSimpleBot = require 'small-simple-bot'

bot = new SmallSimpleBot
    token: process.env.BOT_TOKEN
    dataDir: __dirname + '/data'
    adms: [ username: process.env.BOT_ADM_USERNAME, id: process.env.BOT_ADM_ID ]

captchaToPSLBA = (require 'interaction-new-user-captcha')
    chatId: -1001130370560
    timeoutSecs: 90
    options:
        'coração':  '❤️'
        'cocô':     '💩'
        'fantasma': '👻'
        'gato':     '😺'
        'nenê':     '👶'
        'tomate':   '🍅'
    welcomeTmpl: '''
        Olá <a href="%USER_URI%">%USER_NAME%</a>!

        Bem vinde ao grupo do
        <b>Projeto Software Livre - Bahia</b>

        Você é humano
        e fala português?
        Então, clique
        no %OPT%.

        Você tem %SECS% segundos.'''
    failTmpl: '<a href="%USER_URI%">%USER_NAME%</a> não respondeu ao captcha.'
    successTmpl: 'Bem vinde <a href="%USER_URI%">%USER_NAME%</a>!'

bot.addInteractions(
    #interactionPersonalEmail
    captchaToPSLBA
    require 'interaction-reboot'
    require 'interaction-say-it'
    require 'interaction-tic-tac-toe'
    require 'interaction-youtube'
)

fs.readdir './interactions', (err, files)->
    throw err if err
    bot.addInteractions (
        files
        .filter (f)=> ! f.match /\.test\.coffee$/
        .filter (f)=> f.match /\.coffee$/
        .map (f)=>
            require './interactions/' + f.replace /\.coffee$/, ''
        )...

do bot.run
