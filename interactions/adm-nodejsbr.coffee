{ timeout } = require 'small-simple-bot/helpers/timing'

options =
  'coração': '❤️'
  'cocô': '💩'
  'fantasma': '👻'
  'gato': '😺'
  'nenê': '👶'

emojiOk = '👍' # nunca será uma opção válida. E é um bom honeypot.

optKeys = Object.keys(options)

cbPrefix = 'adm-nodejsBR'

admNodeJsBR = (bot, update)->
    act = update._act
    return if act.chat.id isnt -1001017517994 # NodejsBR
    #return if act.chat.id isnt -1001496821262 # teste
    if act?.new_chat_participant
        humanTestHello bot, act
    if update._kind is 'callback_query' and act.data[..cbPrefix.length-1] is cbPrefix
        humanTestClick bot, act

admNodeJsBR.ignoreTimeout = true
#module.exports = admNodeJsBR
module.exports = disabledAdmNodeJsBR = ()-> false

helloMessages = {}

humanTestHello = (bot, act)->
    return if act.minutesAgo > 2 # testing only here, so humanTestClick can test after a reboot.
    newUser = act.new_chat_participant
    chatId = act.chat.id
    msgId = act.message_id
    name = (newUser.first_name or newUser.username).replace(/\s.*/,'').replace(/[<>&]/g,' ')
    opt = optKeys[ Math.floor( Math.random() * optKeys.length ) ]
    helloKey = Math.random().toString(36).split('.')[1]
    helloMessages[helloKey] = { }
    keyboard = JSON.stringify inline_keyboard: [[
        (for key, emoji of options
            ok = if key is opt then 'OK' else 'FAIL' + key
            {
                text: emoji
                callback_data: "#{cbPrefix}##{newUser.id}##{ok}##{name}##{helloKey}"
            }
        )...
        {
            text: emojiOk
            callback_data: "#{cbPrefix}##{newUser.id}#FAIL##{name}##{helloKey}"
        }
    ]]
    timeoutCaptcha = 60
    mkText = -> """
        <pre><code class="language-javascript">
        function newUser() {

         console.log(\`
          Olá <a href="tg://user?id=#{newUser.id}">#{name}</a>!
          Bem /vind[ao]/gi ao
          grupo Node.js Brasil!
         \`)

         prompt(`
          Você é humano
          e fala português?
          Então, clique
          no #{opt}:
         `)

         setTimeout(BAN, #{timeoutCaptcha}*1000)
        }
        </code></pre>"""
    msgConf = reply_to_message_id: msgId, parse_mode: 'HTML', reply_markup: keyboard
    bot.sendMessage mkText(), chatId, msgConf, (error, response, result)->
        msgId = result?.message_id
        timeout 60*20, -> bot.deleteMessage chatId, msgId
        return if error or not msgId
        countdown = bot.helpers.timing.interval 10, ->
            timeoutCaptcha -= 10
            if timeoutCaptcha > 0
                bot.editMessageText mkText(), chatId, msgId, msgConf
            else
                clearTimeout countdown
                msg = "<a href=\"tg://user?id=#{newUser.id}\">#{name}</a> falhou em responder ao captcha."
                bot.editMessageText msg, chatId, msgId, parse_mode: 'HTML'
                untilDate = 60*60 + Math.round Date.now() / 1000 # uma hora
                bot.kickChatMember chatId, newUser.id, untilDate
                delete helloMessages[helloKey]
        helloMessages[helloKey].countdown = countdown

humanTestClick = (bot, act)->
    chatId = act.chat.id
    msgId = act.message.message_id
    data = act.data.split '#'
    newUsrId = data[1]
    isOK = data[2] is 'OK'
    name = data[3]
    helloKey = data[4]
    if act.from.id.toString() is newUsrId or bot.isAdm act.from.username
        if isOK
            if helloMessages[helloKey]?.countdown
                clearInterval helloMessages[helloKey].countdown
                delete helloMessages[helloKey]
            #bot.deleteMessage chatId, act.message.message_id
            #config = parse_mode: 'HTML'
            #if act.message.reply_to_message?.message_id
            #    config.reply_to_message_id = act.message.reply_to_message?.message_id
            msg = "Bem vind[ao] <a href=\"tg://user?id=#{newUsrId}\">#{name}</a>!"
            bot.editMessageText msg, chatId, msgId, parse_mode: 'HTML'
    else
        clicador = act.from.username or act.from.first_name
        bot.logger.log "#{clicador} tentou validar o humanTest de #{name}."
