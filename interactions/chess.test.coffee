assert = require 'assert'
interaction = require './chess'
{
  pieces, initalGameData, serializeBoard, deserializeBoard, isValidMove
} = interaction._
{
  FakeBot, mkUpdate, mkMsgUpdate, mkCbQueryUpdate
} = require 'small-simple-bot/test/FakeBot'
{ abs } = Math

chessCbQueryUpdate = (data, conf)->
    mkCbQueryUpdate '♔@theuser **Vs** ♚@friend', data, conf

mkBoard = (str)-> str.replace(/ /g,'').split('\n').map (l)=> l.split ''

strInitialBoard = '''
    ♜ ♞ ♝ ♛ ♚ ♝ ♞ ♜
    ♟ ♟ ♟ ♟ ♟ ♟ ♟ ♟
    □ □ □ □ □ □ □ □
    □ □ □ □ □ □ □ □
    □ □ □ □ □ □ □ □
    □ □ □ □ □ □ □ □
    ♙ ♙ ♙ ♙ ♙ ♙ ♙ ♙
    ♖ ♘ ♗ ♕ ♔ ♗ ♘ ♖
'''

assertChessKeyboard = (keyboard, espectedBoard)->
    board = mkBoard espectedBoard
    for y in [0..7]
        for x in [0..7]
            piece = if board[y][x] is '□' then ' ' else board[y][x]
            assert.equal keyboard[y][x].text, piece

arrInitialBoard = mkBoard strInitialBoard
blankBoard = mkBoard '''
    □ □ □ □ □ □ □ □
    □ □ □ □ □ □ □ □
    □ □ □ □ □ □ □ □
    □ □ □ □ □ □ □ □
    □ □ □ □ □ □ □ □
    □ □ □ □ □ □ □ □
    □ □ □ □ □ □ □ □
    □ □ □ □ □ □ □ □
'''
pawnBoxBoard = mkBoard '''
    □ □ □ □ □ □ □ □
    □ □ □ □ □ □ □ □
    □ □ ♟ ♟ ♟ □ □ □
    □ □ ♟ □ ♟ □ □ □
    □ □ ♟ ♟ ♟ □ □ □
    □ □ □ □ □ □ □ □
    □ □ □ □ □ □ □ □
    □ □ □ □ □ □ □ □
'''

mkMoveValidator = (piece, board=blankBoard)->
    oponent = if piece in pieces.player[1] then '♟' else '♙'
    (xFrom, yFrom, xTo, yTo, opts={})->
        boardToPrint = [board.map((l)=>[l...])...]
        boardToPrint[yFrom][xFrom] = '×'
        canMove = not opts.cant
        message = (if canMove then 'Can' else 'Can NOT') + \
            " move #{piece} from #{xFrom},#{yFrom} to #{xTo},#{yTo}\n" + \
            boardToPrint.map((l)=> l.join ' ').join '\n'
        from = { x: xFrom, y: yFrom }
        to = { x: xTo, y: yTo }
        try
            result1 = isValidMove piece, board[yTo][xTo], from, to, board
        catch err
            message += '\n' + err.message
            result1 = false
        if opts.noOponent
            result2 = result1
        else
            try
                result2 = isValidMove piece, oponent, from, to, board
            catch err
                message += '\n' + err.message
                result2 = false
        assert.equal result1, result2, "isValidMove results must to be the same,
          with (#{result2}) and without (#{result1}) an oponent piece. " + message
        assert.equal result1, canMove, message

describe 'Chess', ->

    beforeEach ->
        @bot = new FakeBot

    describe 'Interact', ->

        it 'warn the correct usage', ->
            interaction @bot, mkMsgUpdate '/chess'
            assert.equal @bot.hist[0].type, 'message'
            assert.equal @bot.hist[0].text, 'Use "/xadrez @alguem", então iniciarei um jogo para você e esse alguém.'

        it 'ignore othe commands', ->
            interaction @bot, mkMsgUpdate '/other'
            assert.equal @bot.hist[0], null

        it 'ignore wrong bot', ->
            interaction @bot, mkMsgUpdate '/chess@otherBot'
            assert.equal @bot.hist[0], null

        it 'start a game', ->
            interaction @bot, mkMsgUpdate '/chess @friend'
            assert.equal @bot.hist[0].type, 'message'
            assert.equal @bot.hist[0].text, '''
            ♔@theuser **Vs** ♚@friend
            É a vez de theuser.
            '''
            assert.equal @bot.hist[0].parse_mode, 'Markdown'
            keyboard = @bot.hist[0].reply_markup.inline_keyboard
            assertChessKeyboard keyboard, strInitialBoard
            assert.equal keyboard[0][7].callback_data, 'chess:sel:07,1,>ZsJ////!!!!!!!!!!!!!!!!¥¥¥¥´ÐéÀ'
            assert.equal keyboard[4][4].callback_data, 'chess:sel:44,1,>ZsJ////!!!!!!!!!!!!!!!!¥¥¥¥´ÐéÀ'

        it 'select a piece', ->
            interaction @bot, chessCbQueryUpdate 'chess:sel:60,1,>ZsJ////!!!!!!!!!!!!!!!!¥¥¥¥´ÐéÀ'
            assert.equal @bot.hist[0].type, 'edit_message'
            keyboard = @bot.hist[0].reply_markup.inline_keyboard
            assertChessKeyboard keyboard, strInitialBoard
            assert.equal keyboard[5][0].callback_data, 'chess:mv:60:50,1,>ZsJ////!!!!!!!!!!!!!!!!¥¥¥¥´ÐéÀ'
            assert.equal @bot.hist[1].type, 'answer_cb_query'
            assert.equal @bot.hist[1].text, 'theuser selecionou ♙, em 1,7.'

        it 'move a pawn', ->
            interaction @bot, chessCbQueryUpdate 'chess:mv:60:50,1,>ZsJ////!!!!!!!!!!!!!!!!¥¥¥¥´ÐéÀ'
            assert.equal @bot.hist[0].type, 'edit_message'
            keyboard = @bot.hist[0].reply_markup.inline_keyboard
            assertChessKeyboard keyboard, '''
                ♜ ♞ ♝ ♛ ♚ ♝ ♞ ♜
                ♟ ♟ ♟ ♟ ♟ ♟ ♟ ♟
                □ □ □ □ □ □ □ □
                □ □ □ □ □ □ □ □
                □ □ □ □ □ □ □ □
                ♙ □ □ □ □ □ □ □
                □ ♙ ♙ ♙ ♙ ♙ ♙ ♙
                ♖ ♘ ♗ ♕ ♔ ♗ ♘ ♖
            '''

        it 'blocks ilegal pawn move', ->
            interaction @bot, chessCbQueryUpdate 'chess:mv:60:30,1,>ZsJ////!!!!!!!!!!!!!!!!¥¥¥¥´ÐéÀ'
            assert.equal @bot.hist[0].type, 'answer_cb_query'
            assert.equal @bot.hist[0].text, 'theuser, você não pode mover ♙ para 1,4.'

        it 'blocks a try to move an empty square', ->
            interaction @bot, chessCbQueryUpdate 'chess:sel:44,1,>ZsJ////!!!!!!!!!!!!!!!!¥¥¥¥´ÐéÀ'
            assert.equal @bot.hist[0].type, 'answer_cb_query'
            assert.equal @bot.hist[0].text, 'theuser, selecione uma de suas peças.'
            interaction @bot, chessCbQueryUpdate 'chess:mv:44:55,1,>ZsJ////!!!!!!!!!!!!!!!!¥¥¥¥´ÐéÀ'
            assert.equal @bot.hist[0].type, 'answer_cb_query'
            assert.equal @bot.hist[0].text, 'theuser, você não pode mover uma casa vazia.'

        it 'blocks user trying to kill they own piece', ->
            interaction @bot, chessCbQueryUpdate 'chess:mv:71:63,1,>ZsJ////!!!!!!!!!!!!!!!!¥¥¥¥´ÐéÀ'
            assert.equal @bot.hist[0].type, 'answer_cb_query'
            assert.equal @bot.hist[0].text, 'theuser, você não pode atacar suas peças.'

        it 'blocks user trying to move other players piece', ->
            interaction @bot, chessCbQueryUpdate 'chess:mv:01:02,1,>ZsJ////!!!!!!!!!!!!!!!!¥¥¥¥´ÐéÀ'
            assert.equal @bot.hist[0].type, 'answer_cb_query'
            assert.equal @bot.hist[0].text, 'theuser, você não pode mover uma peça do seu oponente.'

    describe 'Moving validator', ->

        it 'Cant move to the same place', ->
            assert.throws(
                -> isValidMove '♟', '□', {x:0,y:1}, {x:0,y:1}, arrInitialBoard
                name: 'Error'
                message: 'A posição de destino não pode ser a origem.'
            )

        it 'move a pawn', ->
            # one step forward
            assert isValidMove '♟', '□', {x:0,y:1}, {x:0,y:2}, arrInitialBoard
            assert isValidMove '♟', '□', {x:1,y:2}, {x:1,y:3}, arrInitialBoard
            assert isValidMove '♟', '□', {x:2,y:6}, {x:2,y:7}, arrInitialBoard
            # fast first step
            assert isValidMove '♟', '□', {x:0,y:1}, {x:0,y:3}, arrInitialBoard
            assert isValidMove '♟', '□', {x:5,y:1}, {x:5,y:3}, arrInitialBoard
            # can't runn too much
            assert not isValidMove '♟', '□', {x:0,y:1}, {x:0,y:4}, arrInitialBoard
            assert not isValidMove '♟', '□', {x:5,y:1}, {x:5,y:4}, arrInitialBoard
            # can't move to any other direction
            assert not isValidMove '♟', '□', {x:5,y:1}, {x:6,y:2}, arrInitialBoard
            assert not isValidMove '♟', '□', {x:5,y:1}, {x:6,y:1}, arrInitialBoard
            assert not isValidMove '♟', '□', {x:5,y:1}, {x:6,y:0}, arrInitialBoard
            assert not isValidMove '♟', '□', {x:5,y:1}, {x:5,y:0}, arrInitialBoard
            assert not isValidMove '♟', '□', {x:5,y:1}, {x:4,y:0}, arrInitialBoard
            assert not isValidMove '♟', '□', {x:5,y:1}, {x:4,y:1}, arrInitialBoard
            assert not isValidMove '♟', '□', {x:5,y:1}, {x:4,y:2}, arrInitialBoard
            # pawn can't kill with a normal forward move
            assert not isValidMove '♟', '♘', {x:5,y:1}, {x:5,y:2}, arrInitialBoard
            assert not isValidMove '♟', '♘', {x:5,y:1}, {x:5,y:3}, arrInitialBoard
            # pawn can't kill with a backward diagonal
            assert not isValidMove '♟', '♘', {x:5,y:1}, {x:6,y:0}, arrInitialBoard
            assert not isValidMove '♟', '♘', {x:5,y:1}, {x:4,y:0}, arrInitialBoard
            # pawn can kill with a forward diagonal
            assert isValidMove '♟', '♘', {x:5,y:1}, {x:6,y:2}, arrInitialBoard
            assert isValidMove '♟', '♘', {x:5,y:1}, {x:4,y:2}, arrInitialBoard

        it 'move a rook', -> # also know as tower
            validadeMove = mkMoveValidator '♜'
            validadeMoveInABox = mkMoveValidator '♜', pawnBoxBoard
            for y in [0..7]
                for x in [0..7]
                    validadeMoveInABox 3,3, x,y, cant:on, noOponent:on
                    if x is 3 and y is 3
                        # Can't move to the origin square
                    else if x is 3 or y is 3 # Valid horiz/vert
                        validadeMove 3,3, x,y
                    else
                        validadeMove 3,3, x,y, cant:on

        it 'move a knight', -> # also know as horse
            validadeMove = mkMoveValidator '♞'
            validadeMoveInABox = mkMoveValidator '♞', pawnBoxBoard
            for y in [0..7]
                for x in [0..7]
                    if '2,1 4,1 2,5 4,5 1,2 1,4 5,2 5,4'.indexOf(x+','+y) > -1
                        validadeMove 3,3, x,y
                        validadeMoveInABox 3,3, x,y
                    else
                        validadeMove 3,3, x,y, cant:on
                        validadeMoveInABox 3,3, x,y, cant:on

        it 'move a bishop', ->
            validadeMove = mkMoveValidator '♝'
            validadeMoveInABox = mkMoveValidator '♝', pawnBoxBoard
            for y in [0..7]
                for x in [0..7]
                    validadeMoveInABox 3,3, x,y, cant:on, noOponent:on
                    if x is 3 and y is 3
                        # Can't move to the origin square
                    else if abs(x-3) is abs(y-3) # Valid diagonal
                        validadeMove 3,3, x,y
                    else
                        validadeMove 3,3, x,y, cant:on

        it 'move a queen', ->
            validadeMove = mkMoveValidator '♛'
            validadeMoveInABox = mkMoveValidator '♛', pawnBoxBoard
            for y in [0..7]
                for x in [0..7]
                    validadeMoveInABox 3,3, x,y, cant:on, noOponent:on
                    if x is 3 and y is 3
                        # Can't move to the origin square
                    else if abs(x-3) is abs(y-3) # Valid diagonal
                        validadeMove 3,3, x,y
                    else if x is 3 or y is 3 # Valid horiz/vert
                        validadeMove 3,3, x,y
                    else
                        validadeMove 3,3, x,y, cant:on

        it 'move a king', ->
            validadeMove = mkMoveValidator '♚'
            validadeMoveInABox = mkMoveValidator '♚', pawnBoxBoard
            for y in [0..7]
                for x in [0..7]
                    validadeMoveInABox 3,3, x,y, cant:on, noOponent:on
                    if x is 3 and y is 3
                        validadeMove 3,3, x,y, cant:on # Can't move to the origin
                    else if 2 <= x <= 4 and 2 <= y <= 4
                        validadeMove 3,3, x,y
                    else
                        validadeMove 3,3, x,y, cant:on

    describe 'Serialize and deserialize board', ->

        it 'Builds inital game data', ->
            data = initalGameData()
            assert.equal data.turnOwner, 1
            assert.deepStrictEqual data.board, arrInitialBoard

        it 'Serializes initial board', ->
            assert.equal(
                serializeBoard(arrInitialBoard),
                '>ZsJ////!!!!!!!!!!!!!!!!¥¥¥¥´ÐéÀ'
            )

        it 'Deserializes initial board', ->
            assert.deepStrictEqual(
                deserializeBoard('>ZsJ////!!!!!!!!!!!!!!!!¥¥¥¥´ÐéÀ'),
                arrInitialBoard
            )

        it 'Serializes and deserializes random boards', ->
            lastSerealization = ''
            for n in [1..10000]
                process.stdout.write "\r#{n/1000}k boards"
                strBoard = strInitialBoard.split('')
                strBoard.forEach (c, i)->
                    j = 1
                    while strBoard[j].match(/\s/)
                        j = Math.floor Math.random() * strBoard.length
                    unless c.match(/\s/)
                        strBoard[i] = strBoard[j]
                        strBoard[j] = c
                board = mkBoard strBoard.join('')
                serealized = serializeBoard board
                assert.notEqual serealized, lastSerealization
                assert.deepStrictEqual(
                    deserializeBoard(serealized),
                    board
                )

    describe 'End Game', ->

        it 'opens give up confirmation', ->
            interaction @bot, chessCbQueryUpdate 'chess:give-up:open,1,>ZsJ////!!!!!!!!!!!!!!!!¥¥¥¥´ÐéÀ'
            assert.equal @bot.hist[0].type, 'edit_message'
            keyboard = @bot.hist[0].reply_markup.inline_keyboard
            assertChessKeyboard keyboard, strInitialBoard
            assert.deepStrictEqual keyboard[8], [{
                text: 'Confirmar @theuser'
                callback_data: 'chess:give-up:theuser,1,>ZsJ////!!!!!!!!!!!!!!!!¥¥¥¥´ÐéÀ'
            }
            {
                text: 'Cancelar'
                callback_data: 'chess:give-up:cancel,1,>ZsJ////!!!!!!!!!!!!!!!!¥¥¥¥´ÐéÀ'
            }]

        it 'confirmes give up', ->
            interaction @bot, chessCbQueryUpdate 'chess:give-up:theuser,1,>ZsJ////!!!!!!!!!!!!!!!!¥¥¥¥´ÐéÀ'
            assert.equal @bot.hist[0].type, 'edit_message'
            assert.deepStrictEqual @bot.hist[0], {
              chatId: 0
              msgId: 0
              parse_mode: 'Markdown'
              text: '''♔@theuser **Vs** ♚@friend
                       friend venceu! theuser desistiu do jogo.
                       ♜ ♞ ♝ ♛ ♚ ♝ ♞ ♜
                       ♟ ♟ ♟ ♟ ♟ ♟ ♟ ♟
                       □ □ □ □ □ □ □ □
                       □ □ □ □ □ □ □ □
                       □ □ □ □ □ □ □ □
                       □ □ □ □ □ □ □ □
                       ♙ ♙ ♙ ♙ ♙ ♙ ♙ ♙
                       ♖ ♘ ♗ ♕ ♔ ♗ ♘ ♖'''
              type: 'edit_message'
            }

        it 'wrong user tryes to confirm the give up', ->
            interaction @bot, chessCbQueryUpdate(
                'chess:give-up:theuser,1,>ZsJ////!!!!!!!!!!!!!!!!¥¥¥¥´ÐéÀ',
                username: 'friend'
            )
            assert.equal @bot.hist[0].type, 'answer_cb_query'
            assert.equal @bot.hist[0].text, 'Você não pode confirmar a desistência.'

        it 'cancels give up', ->
            interaction @bot, chessCbQueryUpdate 'chess:give-up:cancel,1,>ZsJ////!!!!!!!!!!!!!!!!¥¥¥¥´ÐéÀ'
            assert.equal @bot.hist[1].type, 'answer_cb_query'
            assert.equal @bot.hist[1].text, 'theuser cancelou a desistência.'
            assert.equal @bot.hist[0].type, 'edit_message'
            keyboard = @bot.hist[0].reply_markup.inline_keyboard
            assertChessKeyboard keyboard, strInitialBoard
            assert.deepStrictEqual keyboard[8], [{
              text: 'Desistir'
              callback_data: 'chess:give-up:open,1,>ZsJ////!!!!!!!!!!!!!!!!¥¥¥¥´ÐéÀ'
            }]
