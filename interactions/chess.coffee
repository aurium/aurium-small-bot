fs = require 'fs'
timeout = (secs, callback)-> setTimeout callback, secs*1000
interval = (secs, callback)-> setInterval callback, secs*1000
arr = (str)-> str.join(' ').split /\s+/
{abs} = Math

cmdRE = null

pieces =
    'player':[ null, '♙♖♘♗♕♔', '♟♜♞♝♛♚' ]
    '0':'□'
    '1':'♟', '2':'♜', '3':'♞', '4':'♝', '5':'♛', '6':'♚'
    '7':'♙', '8':'♖', '9':'♘', 'a':'♗', 'b':'♕', 'c':'♔'
    '□':'0'
    '♟':'1', '♜':'2', '♞':'3', '♝':'4', '♛':'5', '♚':'6'
    '♙':'7', '♖':'8', '♘':'9', '♗':'a', '♕':'b', '♔':'c'

charTab = "!\"#$%&'()*+…-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~¡¢£¤¥¦§¨©ª«¬ø®¯°±²³´µ¶·¸¹º»¼½¾¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìí"

###
# Create charTab
charTab = ''
startCharTab = 33
endCharTab = 13*13+34
for i in [startCharTab..endCharTab]
    if i+34 is 173
        charTab += 'ø'
    else if i > 126 # escape crash and white chars
        charTab += String.fromCharCode i+34
    else if String.fromCharCode(i) is ','
        # do not use ",". it is data separator.
        charTab += '…'
    else
        charTab += String.fromCharCode i
console.log('charTab:', charTab.length, JSON.stringify charTab)
###

module.exports = chess = (bot, update)->
    if update._kind is 'callback_query'
        if match = update._act?.data.match /^chess:([^,]+),(.*)/
            return playerAction bot, update._act, match[1], match[2]
    msg = update._act?.text or ''
    chatId = update._act?.chat?.id or 0
    msgId = update._act?.message_id or 0
    usr = update._act?.from.username
    gName = "(?:chess|xadrez)"
    gCmd = "#{gName}(?:@#{bot.username})?"
    cmdRE = ///^\s*/#{gCmd}(?:\s+@([^\s]+))?(?:\s.*)?$///i
    cmd = msg.match cmdRE
    if cmd
        if cmd[1]
            initGame usr, cmd[1], chatId, bot
        else
            bot.sendMessage 'Use "/xadrez @alguem",
            então iniciarei um jogo para você e esse alguém.', chatId, reply_to_message_id: msgId

chess._ = { pieces }

chess._.initalGameData = initalGameData = (player1, player2)->
    {
        players: [null, player1, player2]
        turnOwner: 1
        board: '''
            ♜♞♝♛♚♝♞♜
            ♟♟♟♟♟♟♟♟
            □□□□□□□□
            □□□□□□□□
            □□□□□□□□
            □□□□□□□□
            ♙♙♙♙♙♙♙♙
            ♖♘♗♕♔♗♘♖
            '''.split('\n').map (l)=> l.split ''
    }

chess._.initGame = initGame = (player1, player2, chatId, bot)->
    gameData = initalGameData player1, player2
    bot.sendMessage mkGameText(gameData), chatId,
    parse_mode: 'Markdown',
    reply_markup: mkBoard(gameData),
    (err)=> bot.logErrorAndMsgAdm err, 'Cant init Chess game.' if err

chess._.mkGameText = mkGameText = (gameData, extra='.')-> """
    ♔@#{gameData.players[1]} **Vs** ♚@#{gameData.players[2]}
    É a vez de #{gameData.players[gameData.turnOwner]}#{extra}
    """

chess._.serializeBoard = serializeBoard = (aBoard)->
    s = (for line in aBoard
        (pieces[piece] for piece in line).join ''
    ).join ''
    s=s.replace(/(..)/g,'$1\n').split '\n'
    .map (n)=> charTab[parseInt n, 13]
    .join ''
    s

chess._.deserializeBoard = deserializeBoard = (sBoard)->
    board = []
    line = null
    for c, i in sBoard.split('')
        if i % 4 is 0
            line = []
            board.push line
        num = charTab.indexOf(c).toString(13)
        num = '0' + num if num.length < 2
        [a, b] = num.split('')
        line.push pieces[a], pieces[b]
    return board

chess._.serializeGameData = serializeGameData = (gameData)->
    "#{gameData.turnOwner},#{serializeBoard gameData.board}"

chess._.deserializeGameData = deserializeGameData = (sData, msgTxt)->
    players = msgTxt.match /♔@([^\s]+) .* ♚@([^\s]+)/s
    sData = sData.split ','
    {
        players: [null, players[1], players[2]]
        turnOwner: parseInt sData[0]
        board: deserializeBoard sData[1]
    }

chess._.mkBoard = mkBoard = (gameData, opts={})->
    opts.action ?= 'sel:'
    sData = serializeGameData gameData
    JSON.stringify inline_keyboard: [
        ( for line,y in gameData.board
            ( for piece,x in line
                cmd = "#{opts.action}#{y}#{x}"
                {
                    text: if piece is '□' then ' ' else piece
                    callback_data: "chess:#{cmd},#{sData}"
                }
            )
        )...
        if opts.giveUp
            [
                {
                    text: "Confirmar @#{opts.giveUp}",
                    callback_data: "chess:give-up:#{opts.giveUp},#{sData}"
                }
                {
                    text: 'Cancelar',
                    callback_data: "chess:give-up:cancel,#{sData}"
                }
            ]
        else
            [
                {
                    text: 'Desistir',
                    callback_data: "chess:give-up:open,#{sData}"
                }
            ]
    ]

chess._.playerAction = playerAction = (bot, updateAct, cmd, sData)->
    ctx =
        bot: bot
        queryId: updateAct.id
        chatId: updateAct.chat?.id or 0
        msgId: updateAct.message.message_id or 0
        usr: updateAct.from.username
        gameData: deserializeGameData sData, updateAct.message.text
        cmd: cmd.split ':'
    if ctx.usr is ctx.gameData.players[ctx.gameData.turnOwner] \
       or ctx.cmd[0] is 'give-up'
        playerAction[ctx.cmd[0]] ctx
    else
        turnOwner = ctx.gameData.players[ctx.gameData.turnOwner]
        bot.answerCallbackQuery ctx.queryId, "Ops... Essa é a vez de #{turnOwner}.", true

playerAction.sel = (ctx)->
    turnOwner = ctx.gameData.players[ctx.gameData.turnOwner]
    pos = ctx.cmd[1].split('').map (n)-> parseInt n
    piece = ctx.gameData.board[pos[0]][pos[1]]
    if piece in pieces.player[ctx.gameData.turnOwner]
        ctx.bot.answerCallbackQuery ctx.queryId, "#{turnOwner} selecionou #{piece}, em #{pos[1]+1},#{pos[0]+1}.", false
        ctx.bot.editMessageTextOrReply mkGameText(ctx.gameData, ", que selecionou #{piece}."), ctx.chatId, ctx.msgId,
        parse_mode: 'Markdown', reply_markup: mkBoard(ctx.gameData, action: "mv:#{pos[0]}#{pos[1]}:"),
        (err)=> ctx.bot.logErrorAndMsgAdm err, 'Cant edit Chess game.' if err
    else
        ctx.bot.answerCallbackQuery ctx.queryId, "#{turnOwner}, selecione uma de suas peças.", true

playerAction.mv = (ctx)->
    turnOwner = ctx.gameData.players[ctx.gameData.turnOwner]
    posF = ctx.cmd[1].split('').map (n)-> parseInt n
    posT = ctx.cmd[2].split('').map (n)-> parseInt n
    posF = { y: posF[0], x: posF[1] }
    posT = { y: posT[0], x: posT[1] }
    board = ctx.gameData.board
    pieceM = board[posF.y][posF.x]
    pieceD = board[posT.y][posT.x]
    if pieceM is '□'
        ctx.bot.answerCallbackQuery ctx.queryId, "#{turnOwner}, você não pode mover uma casa vazia.", true
    else if pieceM not in pieces.player[ctx.gameData.turnOwner]
        ctx.bot.answerCallbackQuery ctx.queryId, "#{turnOwner}, você não pode mover uma peça do seu oponente.", true
    else if pieceD in pieces.player[ctx.gameData.turnOwner]
        ctx.bot.answerCallbackQuery ctx.queryId, "#{turnOwner}, você não pode atacar suas peças.", true
    else
        board[posF.y][posF.x] = '□'
        board[posT.y][posT.x] = pieceM
        try
            if isValidMove pieceM, pieceD, posF, posT, board
                if pieceD is '□'
                    ctx.bot.answerCallbackQuery ctx.queryId, "#{turnOwner}
                    moveu #{pieceM}, para #{posT.x+1},#{posT.y+1}.", false
                else
                    ctx.bot.answerCallbackQuery ctx.queryId, "#{turnOwner}
                    matou #{pieceD}, em #{posT.x+1},#{posT.y+1}.", true
                ctx.gameData.turnOwner = if ctx.gameData.turnOwner is 1 then 2 else 1
                ctx.bot.editMessageTextOrReply mkGameText(ctx.gameData), ctx.chatId, ctx.msgId,
                parse_mode: 'Markdown', reply_markup: mkBoard(ctx.gameData),
                (err)=> ctx.bot.logErrorAndMsgAdm err, 'Cant edit Chess game.' if err
            else
                ctx.bot.answerCallbackQuery ctx.queryId, "#{turnOwner},
                você não pode mover #{pieceM} para #{posT.x+1},#{posT.y+1}.", true
        catch err
            ctx.bot.logErrorAndMsgAdm err
            ctx.bot.answerCallbackQuery ctx.queryId, err.message, true

playerAction['give-up'] = (ctx)->
    if ctx.cmd[1] is 'open'
        ctx.bot.editMessageTextOrReply mkGameText(ctx.gameData), ctx.chatId, ctx.msgId,
        parse_mode: 'Markdown', reply_markup: mkBoard(ctx.gameData, giveUp: ctx.usr),
        (err)=> ctx.bot.logErrorAndMsgAdm err, 'Cant edit Chess game.' if err
    else if ctx.cmd[1] is 'cancel'
        ctx.bot.answerCallbackQuery ctx.queryId, "#{ctx.usr} cancelou a desistência.", true
        ctx.bot.editMessageTextOrReply mkGameText(ctx.gameData), ctx.chatId, ctx.msgId,
        parse_mode: 'Markdown', reply_markup: mkBoard(ctx.gameData),
        (err)=> ctx.bot.logErrorAndMsgAdm err, 'Cant edit Chess game.' if err
    else
        if ctx.usr is ctx.cmd[1]
            winner = ctx.gameData.players.find (p)-> p and p isnt ctx.usr
            endGame ctx, winner, "#{ctx.usr} desistiu do jogo."
        else
            ctx.bot.answerCallbackQuery ctx.queryId, "Você não pode confirmar a desistência.", true

chess._.isValidHorizVertMove = isValidHorizVertMove = (posF, posT, board)->
    if posF.x is posT.x
        for y in [posF.y..posT.y] when y not in [posF.y, posT.y]
            return false unless board[y][posF.x] is '□'
        return true
    if posF.y is posT.y
        for x in [posF.x..posT.x] when x not in [posF.x, posT.x]
            return false unless board[posF.y][x] is '□'
        return true
    false

chess._.isValidDiagonalMove = isValidDiagonalMove = (posF, posT, board)->
    return false unless abs(posF.x-posT.x) is abs(posF.y-posT.y)
    for i in [0..posT.x-posF.x]
        return false unless board[posF.y+i][posF.x+i] is '□'
    return true

chess._.isValidMove = isValidMove = (pieceM, pieceD, posF, posT, board)->
    if posF.x is posT.x and posF.y is posT.y
        throw Error "A posição de destino não pode ser a origem."

    p1Pieces = pieces.player[1]
    if pieceD isnt '□' and (pieceM in p1Pieces) is (pieceD in p1Pieces)
        throw Error "Você não pode atacar suas peças."

    if pieceM in arr'♔ ♚'
        return abs(posF.x-posT.x) < 2 and abs(posF.y-posT.y) < 2

    else if pieceM in arr'♕ ♛'
        return isValidHorizVertMove(posF, posT, board) or
               isValidDiagonalMove(posF, posT, board)

    else if pieceM in arr'♗ ♝'
        return isValidDiagonalMove posF, posT, board

    else if pieceM in arr'♖ ♜'
        return isValidHorizVertMove posF, posT, board

    else if pieceM in arr'♘ ♞'
        for [x,y] in [[2,1],[-2,1],[2,-1],[-2,-1],[1,2],[-1,2],[1,-2],[-1,-2]]
            if (posF.x + x) is posT.x and (posF.y + y) is posT.y
                return true
        return false

    else if pieceM is '♙'
        if pieceD isnt '□'
            return abs(posF.x-posT.x) is 1 and posF.y-1 is posT.y
        else if posF.y is 6
            return posF.x is posT.x and 0 < (posF.y-posT.y) < 3
        else
            return posF.x is posT.x and (posF.y-posT.y) is 1

    else if pieceM is '♟'
        if pieceD isnt '□'
            return abs(posF.x-posT.x) is 1 and posF.y+1 is posT.y
        else if posF.y is 1
            return posF.x is posT.x and 0 < (posT.y-posF.y) < 3
        else
            return posF.x is posT.x and (posT.y-posF.y) is 1

    throw Error "Unknown chess piece #{pieceM}."


chess._.testVictory = testVictory = (gameData)->
    return null

chess._.endGame = endGame = (ctx, winner, message)->
    ctx.bot.editMessageTextOrReply """
    ♔@#{ctx.gameData.players[1]} **Vs** ♚@#{ctx.gameData.players[2]}
    #{winner} venceu! #{message}
    #{ctx.gameData.board.map((l)-> l.join ' ').join '\n'}
    """, ctx.chatId, ctx.msgId, parse_mode: 'Markdown'
