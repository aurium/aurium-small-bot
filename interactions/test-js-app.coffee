module.exports = testJsApp = (bot, update)->

    usr = update.message?.from.username or ''
    chatId = update._act?.chat?.id or 0
    msgId = update._act?.message_id or 0
    msg = update._act?.text or ''

    if usr is 'aurium' and msg.indexOf('test-js-app') >= 0

        keyboard = JSON.stringify inline_keyboard: [[
            {
                text: 'Test JS App'
                web_app: {
                    url: "https://aurium.neocities.org/bot-app.html?moment=#{escape(new Date())}"
                }
            }
        ]]

        bot.sendMessage 'Test it.',
            chatId,
            reply_to_message_id: msgId,
            reply_markup: keyboard
