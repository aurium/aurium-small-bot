{URL} = require 'url'
https = require 'https'
sanitizeHtml = require 'sanitize-html'
nodeAPI = null
indexNodeAPI = {}
MarkdownIt = require 'markdown-it'

timeout = (secs, callback)-> setTimeout callback, secs*1000

downlodJSON = (bot, url, callback)->
    domain = (new URL url).hostname
    req = https.get url, (res)=>
        unless 200 <= res.statusCode < 300
            err = Error "Ups... Recebi erro #{res.statusCode} do #{domain}."
            err.statusCode = res.statusCode
            return callback err
        contentType = res.headers['content-type']
        unless /^application\/json/.test contentType
            return callback Error "Ups... Esperava JSON do #{domain}, mas recebi #{contentType}."
        res.setEncoding 'utf8'
        rawData = ''
        res.on 'data', (chunk)=> rawData += chunk
        res.on 'end', ()=>
            try
                callback null, JSON.parse rawData
            catch err
                bot.logger.error err
                callback Error "Ups... Algo errado com o JSON do #{domain}: #{err.message}."
    req.on 'error', (err)=>
        bot.logger.error err
        callback Error "Ups... Algo deu errado enquanto tentava baixar a doc do #{domain}."

buildClassIndex = (defClass, indexPre)->
    defClass.properties?.forEach (defProp)->
        indexNodeAPI["#{indexPre}.#{defProp.name}".toLowerCase()] = defProp
        defProp.isProperty = true
    defClass.methods?.forEach (defMethod)->
        indexNodeAPI["#{indexPre}.#{defMethod.name}".toLowerCase()] = defMethod

buildIndex = ->
    nodeAPI.miscs.forEach (defMisc)->
        indexNodeAPI[defMisc.name.toLowerCase()] = defMisc
    nodeAPI.globals.forEach (defGlobal)->
        indexNodeAPI[defGlobal.name.toLowerCase()] = defGlobal
    nodeAPI.methods.forEach (defRootMethod)->
        indexNodeAPI[defRootMethod.name.toLowerCase()] = defRootMethod
    nodeAPI.classes.forEach (defClass)->
        indexNodeAPI[defClass.name.toLowerCase()] = defClass
        buildClassIndex defClass, defClass.name
    nodeAPI.modules.forEach (defModule)->
        cleanName = defModule.name.replace /_?\(.*\)|[^0-9a-z_]/i,''
        indexPreList = [defModule.name]
        indexPreList.push cleanName unless cleanName is defModule.name
        indexPreList.push 'ssl' if cleanName is 'tls'
        indexPreList.forEach (indexPre)->
            indexNodeAPI[indexPre.toLowerCase()] = defModule
            defModule.miscs?.forEach (defMisc)->
                indexNodeAPI["#{indexPre}.#{defMisc.name}".toLowerCase()] = defMisc
            defModule.globals?.forEach (defGlobal)->
                indexNodeAPI["#{indexPre}.#{defGlobal.name}".toLowerCase()] = defGlobal
            defModule.methods?.forEach (defMethod)->
                indexNodeAPI["#{indexPre}.#{defMethod.name}".toLowerCase()] = defMethod
            defModule.classes?.forEach (defClass)->
                className = defClass.name.split('.')[1]
                indexNodeAPI["#{indexPre}.#{className}".toLowerCase()] = defClass
                buildClassIndex defClass, "#{indexPre}.#{className}"


module.exports = docApiNode = (bot, update)->
    msg = update._act?.text or ''
    msgId = update._act?.message_id or 0
    chatId = update._act?.chat?.id or 0
    return npmDoc {bot, msg, msgId, chatId} if /^\s*\/npm/i.test msg
    return unless ///^\s*\/(apinode|nodeapi)(@#{bot.username})?///i.test msg
    cmd = msg.match ///^\s*\/(apinode|nodeapi)(@#{bot.username})?(\s+([^\s(]+))?///i
    if cmd
        cmd = cmd[4]
        unless cmd
            return bot.sendMessage '''
                Esse comando deve ser acompanhado do nome do módulo, classe e método.
                Exemplos:
                <code>/apinode process</code>
                <code>/apinode fs.readFile</code>
                <code>/apinode http.Server.close</code>
                ''', chatId, reply_to_message_id: msgId, parse_mode: 'HTML'
    if nodeAPI
        showDoc {bot, msgId, chatId, cmd}
    else
        downlodJSON bot, 'https://nodejs.org/api/all.json', (err, json)=>
            if err
                bot.logger.error err
                bot.sendMessage err.message, chatId, reply_to_message_id: msgId
            else
                nodeAPI = json
                do buildIndex
                showDoc {bot, msgId, chatId, cmd}


#module.exports.init = (bot)-> timeout 1, => downlodDoc bot, (err)=> if err
#    bot.logErrorAndMsgAdm err
module.exports.init = (bot)-> timeout 1, =>
    downlodJSON bot, 'https://nodejs.org/api/all.json', (err, json)=>
        if err
            bot.logErrorAndMsgAdm err, 'Cant downlod nodejs.org/api/all.json'
        else
            nodeAPI = json
            do buildIndex

mdIt = (code)->
    md = new MarkdownIt
    md.render(code.replace /<!--[^>]*-->/gim, '')

reNestedTag = /(<[^\/][^>]*>[^<]*)<[^\/][^>]*>/gm
reEmptyTag = /<[^\/][^>]*>\s*<\/[^>]*>/gm
reEmptyLines = /\n\s*(\n\s*)+/gm
cleanHTML = (html, wLimit)->
    html
        .replace /\n/gim, ' '
        .replace /<(\/)?h[1-6][^>]*>/gim, '<$1b>'
        .replace /<br[^>]*>/gim, '\n'
        .replace /<(li|ol)[^>]*>/gim, '\n • '
        .replace /<\/(li|ol)>/gim, ''
        .replace /<\/?(p|ul|div)[^>]*>/gim, '\n \n'
    sanitizeConf = allowedTags: ['b','i','pre','code','a'], allowedAttributes: a: ['href']
    html = sanitizeHtml(html, sanitizeConf).replace(reNestedTag, '$1').replace(reEmptyTag, '')
    html = sanitizeHtml html, sanitizeConf
    html.trim().replace reEmptyLines, '\n\n'
    html = html.replace(/([^a-záàãéíóôõúç0-9_]+)/gim, '\x00$1').split '\x00'
    if wLimit and html.length > wLimit
        html = sanitizeHtml(html[0..wLimit].join(''), sanitizeConf) + '...'
    else
        html = html.join ' '
    html.trim().replace reEmptyLines, '\n\n'

showDoc = ({bot, msgId, chatId, cmd})->
    cmd = cmd.toLowerCase()
    curCmd = cmd
    while not indexNodeAPI[curCmd] and curCmd
        curCmd = curCmd.split('.')[..-2].join '.'
    unless curCmd
        return bot.sendMessage "Desculpe, não encontrei documentação para #{cmd}.",
            chatId, reply_to_message_id: msgId
    txt = ''
    if curCmd isnt cmd
        txt = "Não encontrei <code>#{cmd}</code>, mas sei sobre <code>#{curCmd}</code>.\n\n"
    item = indexNodeAPI[curCmd]
    if item.isProperty
        txt += "Property <code>#{item.name} {#{item.type}}</code>"
    else
        txt += "#{item.type} <code>#{item.textRaw}</code>"
    txt += '\n\n' + cleanHTML item.desc

    bot.sendMessage txt, chatId, reply_to_message_id: msgId, parse_mode: 'HTML', (err)=>
        if err
            bot.msgToAdm "/apinode #{cmd}\n\n#{err.message}"
            bot.sendMessage txt.replace(/<[^>]*>/g, ''), chatId, reply_to_message_id: msgId

npmDoc = ({bot, msg, msgId, chatId})->
    packName = msg.match(///^\s*\/(npm)(@#{bot.username})?(\s+(.*))?///i)[4]
    unless packName
        return bot.sendMessage '''
            Esse comando deve ser acompanhado do nome do módulo.
            Exemplo:
            <code>/npm express</code>
            ''', chatId, reply_to_message_id: msgId, parse_mode: 'HTML'
    packName = packName.toLowerCase().trim()
    if packName.match /\s/
        return bot.sendMessage """
            "#{packName}" não parece ser um nome de módulo.
            """, chatId, reply_to_message_id: msgId, parse_mode: 'HTML'

    downlodJSON bot, "https://registry.npmjs.org/#{packName}", (err, pack)=>
        if err
            if err.statusCode is 404
                bot.sendMessage "Ups... parece que o pacote #{packName} não existe.", chatId, reply_to_message_id: msgId
            else
                bot.logger.error err
                bot.sendMessage err.message, chatId, reply_to_message_id: msgId
        else
            txt = (pack.homepage or "https://www.npmjs.com/package/#{packName}").trim()
            txt += '\n\n' + cleanHTML mdIt(pack.readme), 80
            params =
                reply_to_message_id: msgId
                disable_web_page_preview: 'true'
                parse_mode: 'HTML'
            bot.sendMessage txt, chatId, params, (err)=>
                if err
                    bot.msgToAdm "#{msg}\n\n#{err.message}"
                    bot.sendMessage txt.replace(/<[^>]*>/g, ''), chatId, reply_to_message_id: msgId


