owwwMaaanRE = /\b([rhwu]*o+[wu]+|l+o+l+)(\s|-)+(m+[ea]+n+[oae]*)\b/i

module.exports = owwwMaaan = (bot, update)->

    msg = update._act?.text or ''

    if owwwMaaanRE.test msg
        chatId = update._act?.chat?.id
        opts = reply_to_message_id: update._act?.message_id
        bot.sendVoice __dirname + '/owww-maaan.ogg', chatId, opts
