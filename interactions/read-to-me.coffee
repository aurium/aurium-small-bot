run = require '@aurium/run'
fs = require('fs').promises

chats = []

readToMe = (bot, update)->
  chatsFile = bot.sharedDataDir + '/chats-to-read'
  if chats.length is 0
    fs.readFile chatsFile, 'utf8'
    .then (content)=>
      chats = content.split /[\r\n]+/
      readToMe bot, update # now we can just repeat the message parser
    .catch (err)=>
      bot.logErrorAndMsgAdm err, "Loading #{chatsFile} fail."
    return
  readIt bot, update if shouldRead update._act

readToMe.ignoreTimeout = true
module.exports = readToMe

shouldRead = (act)->
  act ?= {}
  return false unless act.chat
  chatId = act.chat.id?.toString() or 'none'
  msg = act.text or ''
  chatId in chats or msg.match /\b(@?aurium|Aur[ée]lio)\b/i

readIt = (bot, update)->
  msg = update._act.text or ''
  chat = update._act.chat
  chatName = chat.first_name or chat.username or chat.title
  from = update._act.from
  fromName = from.first_name
  fromName = from.username if fromName.length < 3 and from.username
  return unless msg
  speak = "Mensagem de #{fromName} em #{chatName}. #{msg}"
  run 'espeak-ng', '-v', 'mb-br1', '-s', '120', speak
