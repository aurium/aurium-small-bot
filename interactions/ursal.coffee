fs = require 'fs'

module.exports = ursal = (bot, update)->
    msg = update._act?.text or ''
    ursalize bot, update._act if msg.match /^\s*\/ursalize(@AuriumBot)?\s*$/i
    ursalPic bot, update._act if msg.match /^\s*\/ursal(@AuriumBot)?\s*$/i

tr = 
  A: 'ѦД'
  'Ã': 'Д̆'
  'Á': 'Д̆'
  'À': 'Д̆'
  B: 'BЪ'
  E: 'EЭ'
  'É': 'Ё'
  H: 'HЧ'
  K: 'Ѭ'
  M: 'ШЩ'
  N: 'ИЛ'
  O: 'Ѳ'
  'Ó': 'Ө́'
  'Ô': 'Ө̄'
  'Õ': 'Ө̆'
  Q: 'Ф'
  R: 'ЯГ'
  S: 'Ꙅ'
  T: 'Ꚍ'
  U: 'Ц'
  V: 'Ѵ'
  W: 'Ѡ'
  X: 'Ж'
  Y: 'Ψ'

ursalize = (bot, act)->
    origUpdate = act.reply_to_message
    chatId = act.chat?.id
    if origUpdate
        msgId = origUpdate.message_id
        txt = (origUpdate.text or origUpdate.caption).toUpperCase()
        # tr first letter:
        for l, c of tr
            txt = txt.replace ///(\b|\s)#{l}///g, "$1#{c[0]}"
        for l, c of tr
            txt = txt.replace ///([^\b\s])#{l}///g, "$1#{c[1] or c[0]}"
        bot.sendMessage "#{txt} ☭", chatId, reply_to_message_id: msgId
    else
        msgId = act.message_id
        bot.sendMessage 'Esse comando deve ser usado em resposta a uma mensagem, camarada.',
            chatId, reply_to_message_id: msgId

ursalPic = (bot, act)->
    chatId = act.chat?.id
    fs.readdir __dirname + '/URSAL/pics', (err, files)->
        i = Math.floor Math.random() * files.length
        pic = "#{__dirname}/URSAL/pics/#{files[i]}"
        bot.sendPhoto pic, chatId

