fs = require('fs').promises
cmdRE = /^\/([-_a-z]+)/i
userMemo = {}
run = require '@aurium/run'

join = (list, lastConnector='and')->
    list = [list...]
    return '' if list.length is 0
    return list[0] if list.length is 1
    last = do list.pop
    "#{list.join(', ')} #{lastConnector} #{last}"

tagsDir = (bot)-> bot.sharedDataDir + '/tags'

tagsFile = (bot, userId)->
    "#{tagsDir bot}/#{userId}.json"

userName = (target)-> target.username or target.first_name or '<unamed>'

extractTagsFromCmd = (text)-> (
    text.trim().toLowerCase().match(/^\/[-_a-z@]+\s+(.*)/i)?[1].split /\s+/
    ) or []

ensureTagsDir = (bot)->
    fs.stat tagsDir bot
    .then (stat)->
        throw Error '"<data-dir>/tags" is not a directory.' unless stat.isDirectory()
    .catch (err)->
        if err.code is 'ENOENT'
            fs.mkdir tagsDir bot
        else
            throw err

readTagsOf = (bot, userId)->
    ensureTagsDir bot
    .then ()-> fs.readFile tagsFile bot, userId
    .then (json)-> JSON.parse json

writeMemoFile = (bot, userId, memo)->
    fs.writeFile tagsFile(bot, userId), JSON.stringify memo, null, '  '

getUserNames = (bot, userIds)->
    Promise.all userIds.map (id)-> getUserName bot, id

getUserName = (bot, userId)-> new Promise (resolve, reject)->
    userId = userId.toString()
    return resolve userMemo[userId] if userMemo[userId]
    bot.getChat userId, (error, response, result)->
        userMemo[userId] = if error
        then "usr(#{userId})"
        else result.username or result.first_name or result.title
        resolve userMemo[userId]

escapeHTML = (code)->
    code
    .replace /&/g, '&amp;'
    .replace /</g, '&lt;'
    .replace />/g, '&gt;'

module.exports = tagMember = (bot, update)->
    msg = (update._act?.text or '').trim()
    return unless msg[0] is '/'
    cmd = msg.match(cmdRE)?[1].toLowerCase().replace /_/g, '-'
    addTag bot, update      if cmd in ['tag', 'add-tag']
    showTags bot, update    if cmd in ['tags', 'showtags', 'show-tags']
    showAllTags bot, update if cmd in ['alltags', 'all-tags', 'chattags', 'chat-tags']
    rmTag bot, update       if cmd in ['rmtag', 'rm-tag']

# Register a tag to an user, on the chat where the command was called.
# Tags defined in a group can't be listed in another one.
addTag = (bot, update)->
    chatId = update._act.chat.id
    msgId = update._act.message_id or 0
    tags = extractTagsFromCmd update._act.text
    from = update._act.from
    replyTo = update._act.reply_to_message or {}
    target = replyTo.from
    origMsgId = replyTo.message_id
    if (tags.length is 0) or not target
        bot.sendMessage """
        Bad member tag command\\.
        You must replay to a message from the user you want to tag with the format:
        `/tag tag1 tag2 tagN`
        """, chatId, reply_to_message_id: msgId, parse_mode: 'MarkdownV2'
    else
        readTagsOf bot, target.id
        .catch (err)->
            if err.code is 'ENOENT' then {} else throw err
        .then (memo)->
            chatMemo = memo[chatId] ?= {}
            for tag in tags
                chatMemo[tag] ?= []
                chatMemo[tag].push from.id unless from.id in chatMemo[tag]
            writeMemoFile bot, target.id, memo
        .then ()->
            bot.sendMessage "
            You just add #{join tags.map((t)->"\"<b>#{escapeHTML t}</b>\""), 'and'}
            #{ if tags.length is 1 then 'tag' else 'tags' }
            to #{userName target}.
            ", chatId, reply_to_message_id: msgId, parse_mode: 'HTML'
        .catch (err)->
            bot.logErrorAndMsgAdm err, "The command \"#{update._act.text}\" fails."
            bot.sendMessage "Ups... Tagging fail.", chatId, reply_to_message_id: msgId

rmTag = (bot, update)->
    chatId = update._act.chat.id
    msgId = update._act.message_id or 0
    tags = extractTagsFromCmd update._act.text
    from = update._act.from
    replyTo = update._act.reply_to_message or {}
    target = replyTo.from
    origMsgId = replyTo.message_id
    if (tags.length is 0) or not target
        bot.sendMessage """
        Bad remove member tag command\\.
        You must replay to a message from the user you want to remove some tag with the format:
        `/rm-tag tag1 tag2 tagN`
        """, chatId, reply_to_message_id: msgId, parse_mode: 'MarkdownV2'
    else
        removed = []
        rmErrors = []
        readTagsOf bot, target.id
        .catch (err)->
            if err.code is 'ENOENT' then {} else throw err
        .then (memo)->
            chatMemo = memo[chatId] ?= {}
            for tag in tags
                if chatMemo[tag]
                    index = chatMemo[tag].indexOf from.id
                    if index is -1
                        rmErrors.push "\n- You do not set \"#{tag}\" tag to this user."
                    else
                        chatMemo[tag] = chatMemo[tag].filter (id)-> id isnt from.id
                        delete chatMemo[tag] if chatMemo[tag].length is 0
                        removed.push tag
                else
                    rmErrors.push "\n- There is no \"#{tag}\" tag."
            writeMemoFile bot, target.id, memo
        .then ()->
            if removed.length > 0
                msg = "You just remove
                #{join removed.map((t)->"\"#{t}\""), 'and'}
                #{ if removed.length is 1 then 'tag' else 'tags' }"
            else
                msg = "You removed no tags"
            failMsg = ''
            if rmErrors.length > 0
                failMsg = "\n\nFails:#{rmErrors.join ''}"
            bot.sendMessage "#{msg} from #{userName target}.#{failMsg}", chatId, reply_to_message_id: msgId
        .catch (err)->
            bot.logErrorAndMsgAdm err, "The command \"#{update._act.text}\" fails."
            bot.sendMessage "Ups... Removeing tag fail.", chatId, reply_to_message_id: msgId


# Show only tags defined in the chat where the command was called.
showTags = (bot, update)->
    chatId = update._act.chat.id
    msgId = update._act.message_id or 0
    replyTo = update._act.reply_to_message or {}
    target = replyTo.from
    unless target
        bot.sendMessage """
        Bad tag list command\\.
        You must replay to a message from the user you want to list tags with the format:
        `/show-tags`
        """, chatId, reply_to_message_id: msgId, parse_mode: 'MarkdownV2'
    else
        tags = []
        readTagsOf bot, target.id
        .then (memo)->
            taggers = []
            tags = Object.entries(memo[chatId] or {}).map ([tag, users])->
                for userId in users
                    taggers.push userId unless userId in taggers
                times = if users.length > 1 then '×' + users.length else ''
                "\"<b>#{escapeHTML tag}</b>\"#{times}"
            taggers
        .then (taggers)->
            getUserNames bot, taggers
        .then (taggers)->
            bot.sendMessage "
            #{userName target} was tagged with #{join tags, 'and'}.
            \nBy #{join taggers, 'and'}.
            ", chatId, reply_to_message_id: msgId, parse_mode: 'HTML'
        .catch (err)->
            if err.code is 'ENOENT'
                bot.sendMessage "
                #{userName target} was not tagged.
                ", chatId, reply_to_message_id: msgId
            else
                bot.logErrorAndMsgAdm err, "The command \"#{update._act.text}\" fails."
                bot.sendMessage "Ups... Tag listing fail.", chatId, reply_to_message_id: msgId


# Show all tags defined in the chat where the command was called.
showAllTags = (bot, update)->
    chatId = update._act.chat.id
    msgId = update._act.message_id or 0
    do ()->
        output = await run 'grep', '-rl', '"'+chatId+'":', tagsDir bot
        users = await Promise.all(
            output
            .trim()
            .split('\n')
            .map (f)-> f.match(/^.*\/(-?[0-9]+).json$/)[1]
            .map (id, index)->
                id: id
                name: await getUserName bot, id
                memo: await readTagsOf bot, id
        )
        msgs = for { id:userId, name, memo } in users
            tags = Object.entries(memo[chatId]).map ([tag, users])->
                times = if users.length > 1 then '×' + users.length else ''
                "\"<b>#{escapeHTML tag}</b>\"#{times}"
            "\n • #{name} was tagged with #{join tags, 'and'}."
        bot.sendMessage "
        Tagged users on this chat:\n #{msgs.join ''}
        ", chatId, reply_to_message_id: msgId, parse_mode: 'HTML'
    .catch (err)->
        bot.logErrorAndMsgAdm err, "The command \"#{update._act.text}\" fails."
        bot.sendMessage "Ups... Tag listing fail.", chatId, reply_to_message_id: msgId
