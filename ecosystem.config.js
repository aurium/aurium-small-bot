const path = require('path');

module.exports = {
  apps : [{
    name: 'Aurium-Bot',
    script: 'aurium-bot.coffee',

    // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
    interpreter: path.resolve(__dirname, 'node_modules/.bin/coffee'),
    args: null,
    instances: 1,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    log: path.resolve(__dirname, 'log/bot.log'),
    env: {
      NODE_ENV: 'development'
    },
    env_production: {
      NODE_ENV: 'production'
    }
  }]
};
